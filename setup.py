#!/usr/bin/env python3

from pathlib import Path
from setuptools import setup, find_packages

here = Path(__file__).resolve().parent

setup(
    name='macarena',
    version='0.1.0',
    description='Nginx deployment',
    url='https://github.com/messa/macarena',
    author='Petr Messner',
    author_email='petr.messner@gmail.com',
    classifiers=[
        'Programming Language :: Python :: 3.4',
    ],
    keywords='deployment nginx',
    packages=find_packages(exclude=['test*']),
    install_requires=[
        'blessings', # TODO - move to test requirements
        'uwsgi',
        'pyyaml',
    ],
    entry_points={
        'console_scripts': [
            'macarena=macarena:main',
        ],
    },
)
