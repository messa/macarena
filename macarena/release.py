
import logging
from pathlib import Path
import os
from string import Template
import subprocess
from time import sleep, time
import threading

from .util import terminate_and_wait


logger = logging.getLogger(__name__)


class Release:

    def __init__(self, config, release_name):
        self.logger = logger.getChild(self.__class__.__name__)
        self.config = config
        self.release_name = release_name
        self.release_dir = self.config.releases_dir / self.release_name
        self.process = None
        self.socket_id = self.generate_socket_id()
        self.started = False
        self.failed = False

    @property
    def socket_path(self):
        if isinstance(self.config.socket_path, Path):
            self.config.socket_path = str(self.config.socket_path)
        assert isinstance(self.config.socket_path, str)
        sp = Template(self.config.socket_path).substitute(socket_id=self.socket_id)
        return Path(sp)

    @property
    def fifo_path(self):
        sp = str(self.socket_path)
        if sp.endswith('.sock'):
            return Path(sp[:-5] + '.fifo')
        else:
            raise Exception("Don't know how to create fifo path from {!r}".format(sp))

    def prepare(self):
        try:
            self.logger.info('Preparing release dir: %s', self.release_dir)
            self.release_dir.mkdir()
            self.run_prepare_script()
        except:
            self.failed = True
            raise

    def run_prepare_script(self):
        assert self.config.shell.is_file()
        p = subprocess.Popen(str(self.config.shell),
            cwd=str(self.release_dir),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True)
        self.logger.info('Running prepare script (pid %s)', p.pid)
        try:
            def log_stdout():
                while True:
                    line = p.stdout.readline()
                    if not line:
                        break
                    assert isinstance(line, str)
                    line = line.rstrip()
                    self.logger.info('Prepare (%s): %s', p.pid, line)
            threading.Thread(target=log_stdout).start()
            p.stdin.write('set -ex\n')
            p.stdin.write(self.config.prepare_script)
            p.stdin.close()
            p.wait()
            if p.returncode != 0:
                raise Exception('Prepare script failed - return code is {}'.format(p.returncode))
            self.logger.info('Prepare script done')
        finally:
            terminate_and_wait(p)

    def start(self):
        assert not self.started
        assert self.process is None
        env = dict(os.environ)
        env['SOCKET_PATH'] = str(self.socket_path)
        env['FIFO_PATH'] = str(self.fifo_path)
        self.process = subprocess.Popen(str(self.config.shell),
            cwd=str(self.release_dir),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True,
            env=env)
        self.logger.info('Running run script (pid %s)', self.process.pid)
        try:
            def log_stdout():
                while True:
                    line = self.process.stdout.readline()
                    if not line:
                        break
                    assert isinstance(line, str)
                    line = line.rstrip()
                    self.logger.info('Run (%s): %s', self.process.pid, line)
            threading.Thread(target=log_stdout).start()
            self.process.stdin.write('set -ex\n')
            self.process.stdin.write(self.config.run_script)
            self.process.stdin.close()
            sleep(1) # magic number sleep :)
            if self.process.poll() is not None:
                raise Exception('Run script failed - returned with code {}'.format(self.process.returncode))
            self.logger.info('Release run script apparently running OK')
            self.started = True
        except Exception as e:
            terminate_and_wait(self.process)
            self.process = None
            raise

    def start_in_thread(self):
        threading.Thread(target=self.start).start()

    def prepare_and_start(self):
        try:
            self.prepare()
            self.start()
        except:
            self.failed = True
            raise

    def prepare_and_start_in_thread(self):
        threading.Thread(target=self.prepare_and_start).start()

    def close(self):
        if self.process:
            terminate_and_wait(self.process)
            self.process = None

    def generate_socket_id(self):
        return str(int(time()*1000))
