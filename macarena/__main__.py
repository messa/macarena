
import argparse
import logging
import sys

from . import config_from_yaml, Deployer


logger = logging.getLogger(__name__)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('config', metavar='CONFIGFILE', help='path to configuration file')
    args = p.parse_args()
    cfg = config_from_yaml(args.config)
    if cfg.log:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(name)s %(levelname)s: %(message)s',
            filename=cfg.log)
    else:
        # no logfile - log to stderr
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(name)s %(levelname)s: %(message)s')
    try:
        d = Deployer(cfg)
        d.run_service()
    except Exception as e:
        logger.exception('Caught exception: %r', e)
        sys.exit('ERROR: {}'.format(e))


if __name__ == '__main__':
    main()
