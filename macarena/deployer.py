
from datetime import datetime
import logging
import os
from pathlib import Path
import signal
from string import Template
import subprocess
import sys
from time import sleep
import threading

from .release import Release
from .util import terminate_and_wait


logger = logging.getLogger(__name__)


class Deployer:

    def __init__(self, config):
        self.logger = logger.getChild(self.__class__.__name__)
        self.config = config
        if not self.config.shell.is_file():
            raise Exception('shell is not a file')
        if not self.config.releases_dir.is_dir():
            raise Exception('releases_dir is not a directory')
        self.current_release = None
        self.new_release = None
        self.old_releases = []
        self.reload = False
        self.redeploy = False

    def setup_signals(self):
        signal.signal(signal.SIGHUP, self.handle_sighup)
        signal.signal(signal.SIGINT, self.handle_sigint)
        signal.signal(signal.SIGUSR1, self.handle_sigusr1)

    def handle_sighup(self, signal_numbber, stack_frame):
        print('Got HUP')
        self.reload = True

    def handle_sigint(self, signal_number, stack_frame):
        print('Got INT')
        sys.exit(0)

    def handle_sigusr1(self, signal_number, stack_frame):
        print('Got SIGUSR1')
        self.redeploy = True

    def run_service(self):
        self.setup_signals()
        try:
            try:
                current_link = self.config.releases_dir / self.config.current_link_name
                try:
                    release_name = os.readlink(str(current_link))
                except FileNotFoundError:
                    release_name = None
                if release_name:
                    if '/' in release_name:
                        raise Exception('Invalid current link')
                    self.current_release = Release(self.config, release_name)
                else:
                    self.logger.info('No current release found, preparing a new one')
                    self.current_release = Release(self.config, self.generate_release_name())
                    self.current_release.prepare()
                self.current_release.start()
                changed = self.write_nginx_upstream_conf()
                if changed:
                    self.reload_nginx()
                while True:
                    sleep(.1)
                    if self.redeploy:
                        print('Redeploying')
                        self.new_release = Release(self.config, self.generate_release_name())
                        self.new_release.prepare_and_start_in_thread()
                        self.redeploy = False
                        self.reload = False
                    if self.reload:
                        print('Reloading')
                        self.reload = False
                        self.new_release = Release(self.config, self.current_release.release_name)
                        self.new_release.start_in_thread()
                    if self.new_release:
                        if self.new_release.failed:
                            self.logger.warning('new_release.failed; not reloading or redeploying')
                            threading.Thread(target=self.new_release.close).start()
                            self.new_release = None
                        elif self.new_release.started:
                            print('Replacing')
                            old_release = self.current_release
                            self.current_release = self.new_release
                            self.new_release = None
                            self.write_nginx_upstream_conf()
                            try:
                                self.reload_nginx()
                            except Exception as e:
                                self.logger.warning('Failed to reload Nginx (%r), rollbacking to previous release', e)
                                threading.Thread(target=self.current_release.close).start()
                                self.current_release = old_release
                            else:
                                self.old_releases.append(old_release)
                                sleep(1)
                                threading.Thread(target=old_release.close).start()
                                del old_release

            except Exception as e:
                self.logger.exception('Caught exception in run_service: %r', e)
                raise

        finally:
            if self.current_release:
                self.current_release.close()

    def reload_nginx(self):
        assert self.config.shell.is_file()
        assert self.current_release
        p = subprocess.Popen(str(self.config.shell),
            cwd=str(self.current_release.release_dir),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True)
        self.logger.info('Running nginx reload script (pid %s)', p.pid)
        try:
            def log_stdout():
                while True:
                    line = p.stdout.readline()
                    if not line:
                        break
                    assert isinstance(line, str)
                    line = line.rstrip()
                    self.logger.info('Reload Nginx (%s): %s', p.pid, line)
            threading.Thread(target=log_stdout).start()
            p.stdin.write('set -ex\n')
            p.stdin.write(self.config.nginx_reload_script)
            p.stdin.close()
            p.wait()
            if p.returncode != 0:
                raise Exception('Nginx reload script failed - return code is {}'.format(p.returncode))
            self.logger.info('Nginx reload script done')
        finally:
            terminate_and_wait(p)

    def generate_release_name(self):
        return datetime.utcnow().strftime('%Y%m%d%H%M%S')

    def write_nginx_upstream_conf(self):
        assert self.current_release
        up_conf_path = self.config.nginx_upstream_conf_path
        body = Template(self.config.nginx_upstream_template).substitute(
            upstream_name=self.config.nginx_upstream_name,
            socket_path=self.current_release.socket_path)
        try:
            with up_conf_path.open() as f:
                current_body = f.read()
        except FileNotFoundError:
            current_body = None
        if current_body == body:
            self.logger.info('Nginx upstream conf file %s is already up-to-date', up_conf_path)
            return False
        self.logger.info('Writing Nginx upstream conf file %s', up_conf_path)
        temp_path = Path(str(up_conf_path) + '.temp')
        with temp_path.open('w') as f:
            f.write(body)
        try:
            up_conf_path.unlink()
        except FileNotFoundError:
            pass
        temp_path.rename(up_conf_path)
        return True
