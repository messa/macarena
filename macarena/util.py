
import logging
import multiprocessing
import signal
import subprocess


logger = logging.getLogger(__name__)


def terminate_and_wait(process):
    if isinstance(process, subprocess.Popen):
        try:
            if process.poll() is None:
                logger.info('Terminating process %s', process.pid)
                process.send_signal(signal.SIGINT) # instead of process.terminate()
                process.wait()
        except ProcessLookupError:
            pass
    elif isinstance(process, multiprocessing.Process):
        try:
            if process.is_alive():
                logger.info('Terminating process %s', process.pid)
                process.terminate()
                process.join()
        except ProcessLookupError:
            pass
    else:
        raise Exception('Unknown process type')
