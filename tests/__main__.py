
from blessings import Terminal
import logging
import traceback

from . import all_tests


def main():
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(name)s %(levelname)5s: %(message)s')
    t = Terminal()
    for test in all_tests:
        mod, name = test.__module__, test.__name__
        print('{mod}.{name}...'.format(mod=t.black_bold(mod), name=t.white_bold(name)))
        try:
            test()
        except Exception as e:
            print(t.red_bold(traceback.format_exc()))
        else:
            print(t.green_bold('OK'))




if __name__ == '__main__':
    main()
