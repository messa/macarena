
from contextlib import contextmanager
from datetime import datetime
import logging
import multiprocessing
import os
from pathlib import Path
from pprint import pprint
import random
import signal
from shutil import copy, rmtree
from string import Template
import subprocess
import sys
from textwrap import dedent
from time import sleep, time
import threading
from urllib.request import urlopen

from macarena import Config, Deployer
from macarena.util import terminate_and_wait

from . import test, data_dir


logger = logging.getLogger(__name__)


@contextmanager
def test_work_dir(name='test-workdir'):
    work_dir = Path(name)
    if work_dir.is_dir():
        rmtree(str(work_dir))
    work_dir.mkdir()
    try:
        yield work_dir.resolve()
        rmtree(str(work_dir))
    finally:
        pass


@test
def get_uwsgi_path():
    path = Path(sys.prefix) / 'bin' / 'uwsgi'
    assert path.is_file()
    return str(path)


@test
def test_overall_functionality():
    with test_work_dir() as work_dir:
        repo_dir = work_dir / 'repo'
        releases_dir = work_dir / 'releases'
        repo_dir.mkdir()
        releases_dir.mkdir()
        # prepare nginx.conf
        nginx_port = 8900
        nginx_conf = Template(dedent('''
            daemon off;
            worker_processes 2;
            pid $work_dir/nginx.pid;
            error_log $work_dir/error.log;
            events {
                worker_connections 768;
            }
            http {
                access_log $work_dir/access.log;
                include $work_dir/nginx-upstream.conf;
                server {
                    listen $port default;
                    server_name localhost;
                    root /var/www/default;
                    location / {
                        uwsgi_pass up;
                        include /etc/nginx/uwsgi_params;
                    }
                }
            }
        ''')).substitute(work_dir=work_dir, port=nginx_port)
        with (work_dir / 'nginx.conf').open('w') as f:
            f.write(nginx_conf)
        nginx_upstream_conf = dedent('''
                upstream up {
                    server unix:/dev/null;
                }
            ''')
        with (work_dir / 'nginx-upstream.conf').open('w') as f:
            f.write(nginx_upstream_conf)
        np = subprocess.Popen(
            [
                '/usr/sbin/nginx',
                '-c', str(work_dir / 'nginx.conf'),
            ])
        print('Nginx running on port {port} as pid {pid}'.format(port=nginx_port, pid=np.pid))
        assert np.poll() is None
        sleep(1)
        assert np.poll() is None
        try:
            # simulate a git repo
            run('cd {repo_dir} && git init'.format(repo_dir=repo_dir))
            run('cp {data_dir}/hello_app.py {repo_dir}'.format(data_dir=data_dir, repo_dir=repo_dir))
            run('cd {repo_dir} && git add . && git commit -m first'.format(repo_dir=repo_dir))
            mcfg = Config(
                prepare_script='''
                        # git fetch etc. would be here
                        (cd {repo_dir} && git archive master) | tar xv
                    '''.format(
                        repo_dir=repo_dir,
                        uwsgi=get_uwsgi_path()),
                run_script='''
                        exec {uwsgi} \
                            --master \
                            --vacuum \
                            --logto uwsgi.log \
                            --uwsgi-socket $SOCKET_PATH \
                            --master-fifo $FIFO_PATH \
                            --module hello_app
                    '''.format(
                        repo_dir=repo_dir,
                        uwsgi=get_uwsgi_path()),
                releases_dir=releases_dir,
                socket_path=work_dir / 'uwsgi.$socket_id.sock',
                nginx_upstream_conf_path=work_dir / 'nginx-upstream.conf',
                nginx_upstream_name='up',
                nginx_reload_script='''
                        kill -HUP {nginx_pid}
                    '''.format(
                        nginx_pid=np.pid),
            )
            m = Deployer(mcfg)
            mp = multiprocessing.Process(target=m.run_service)
            mp.start()
            try:
                sleep(1.5)
                assert mp.is_alive()
                assert mp.is_alive()
                data = urlopen('http://127.0.0.1:{port}/'.format(port=nginx_port)).read()
                assert data == b'Hello', repr(data)
                print('Test request OK')
                os.kill(mp.pid, signal.SIGHUP)
                print('Sent SIGHUP to deployer')
                for i in range(10):
                    data = urlopen('http://127.0.0.1:{port}/'.format(port=nginx_port)).read()
                    assert data == b'Hello', repr(data)
                    print('Test request OK')
                    sleep(.5)
                os.kill(mp.pid, signal.SIGUSR1)
                print('Sent SIGUSR1 to deployer')
                for i in range(10):
                    data = urlopen('http://127.0.0.1:{port}/'.format(port=nginx_port)).read()
                    assert data == b'Hello', repr(data)
                    print('Test request OK')
                    sleep(.5)
            finally:
                terminate_and_wait(mp)
        finally:
            terminate_and_wait(np)


def run(cmd):
    assert isinstance(cmd, str)
    print('Running {cmd}'.format(cmd=cmd))
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        tail(p.stdout, 'stdout')
        tail(p.stderr, 'stderr')
        p.wait()
        if p.returncode != 0:
            raise Exception('Command {cmd!r} failed: {rc}'.format(
                cmd=cmd, rc=p.returncode))
    finally:
        terminate_and_wait(p)


@test
def test_run():
    run('true')



def tail(pipe, name):
    def w():
        # "for line in pipe:" can buffer sometimes, better use while/redline
        while True:
            line = pipe.readline()
            if not line:
                break
            line = line.decode('UTF-8').rstrip()
            print('{name}: {line}'.format(name=name, line=line))
    threading.Thread(target=w, daemon=True).start()
