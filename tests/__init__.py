
from pathlib import Path

all_tests = []

here = Path(__file__).resolve().parent
data_dir = here / 'data'
assert data_dir.is_dir()
del here


def test(f):
    '''
    Decorator
    '''
    all_tests.append(f)
    return f


from . import overall
