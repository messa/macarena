
# Makefile for development and testing purposes

pyvenv=pyvenv-3.4

all: venv/packages-installed check

check: venv/packages-installed
	venv/bin/python3 -B -m tests

venv:
	$(pyvenv) venv

venv/packages-installed: venv setup.py
	venv/bin/pip install -e .
	touch venv/packages-installed

clean:
	rm -rfv */__pycache__ venv *.egg-info

.PHONY: all check clean
